# Maps en Go
Un map nos permite almacenar información de forma similar a como lo hacíamos con los arreglos o los slices pero con la particularidad de que cada elemento de un map tiene asociada una llave única.

## Conceptos generales
Los maps son unas de las estructuras más utilizadas en Golang gracias a que es muy sencillo utilizarlos y son poderosos métodos de gestión de elementos de conjuntos. A continuación se presenta la sintaxis de la declaración de un map:

```
Mapa = "map" "[" TipoLlave "]" TipoElemento
TipoLlave = Tipo
```
En el siguiente ejemplo declaro un map que almacenará días de la semana donde la llave única será un número entero (correspondiente al orden de los días) y el elemento principal (el que no es la llave única) será una cadena de texto:

```go
var diasSemana map[int]string
```
El map anterior ha sido declarado, pero no ha sido inicializado, si quisiéramos añadir algún elemento, ocurriría un error ya que el elemento sigue siendo nulo. Para inicializar el map utilizamos make:

```go
diasSemana = make(map[int]string)
```
Para insertar elementos en el map anterior (uno a uno):

```go
diasSemana[1] = "Domingo"
diasSemana[2] = "Lunes"
diasSemana[3] = "Martes"
diasSemana[4] = "Miércoles"
diasSemana[5] = "Jueves"
diasSemana[6] = "Viernes"
diasSemana[7] = "Sábado"
```
Si queremos recorrer todos los elementos, podemos hacer lo siguiente:

```go
for dia := range diasSemana {
  fmt.Println("El día", dia, "de la semana es", diasSemana[dia])
}
```
Para buscar un elemento es necesario utilizar una asignación doble en donde la primera variable almacenará el resultado de la consulta y la segunda variable (generalmente llamada ok<) será inicializada con un valor bool true si el elemento existe:

```go
string_dia, ok := diasSemana[8]
if(ok){
  fmt.Println("El elemento sí existe y almacena:", string_dia)
}else{
  fmt.Println("El elemento no existe")
}
```
Para consultar el tamaño de un map se utiliza la función len() como lo hacíamos con slices:

```go
len(diasSemana)
```
La función delete() permite eliminar elementos de un map a través de la llave única de estos:

```go
delete(diasSemana, 2) //Elimina el elemento con la llave 2
```
Ejemplo:
```go
package main
import "fmt"

func main(){

  var diasSemana map[int]string
  diasSemana = make(map[int]string)
  diasSemana[1] = "Domingo"
  diasSemana[2] = "Lunes"
  diasSemana[3] = "Martes"
  diasSemana[4] = "Miércoles"
  diasSemana[5] = "Jueves"
  diasSemana[6] = "Viernes"
  diasSemana[7] = "Sábado"
  //Busca elemento con llave 8
  string_dia, encontrado := diasSemana[8]
  if(encontrado){
    fmt.Println("El día 2 es", string_dia)
  }else{
    fmt.Println("El día 2 no fue encontrado")
  }
  //Eliminar el elemento con llave 2
  delete(diasSemana, 2)
  fmt.Println("Tamaño del map:", len(diasSemana))
}
```
Resultado:

![](../Imagenes/maps.png)

Nota: Gol no considera un error de compilación el tratar de añadir un elemento nuevo con una llave que ya existe en el map, en su lugar simplemente reemplaza el elemento antiguo con la información del nuevo.




***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).