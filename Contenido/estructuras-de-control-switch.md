# Estructura de contol: switch

La sentencia switch permite evaluar una variable de prueba contra una lista de valores conocidos como casos (case). En Go un switch puede ser de dos tipos posible:

* Expresión – Los casos contienen expresiones que son comparadas con el valor de la variable de prueba.
* Tipo – Los casos contienen un tipo de dato que es comparado con el tipo de dato de la variable de prueba.

## Switch de expresiones

Las siguientes reglas aplican para los switch que evalúan expresiones:

* Si no se pasa una expresión, el valor por defecto es true.
* Puede haber cualquier cantidad de casos siempre y cuando cada caso esté seguido de al menos un valor contra el cual comparar, y dos puntos.
* Si se usa una expresión constante para un caso, debe de ser del mismo tipo que el valor de prueba, y debe de ser una constante o una literal.
* Cuando la variable evaluada es igual a un caso, el código después de los dos puntos se ejecuta. No se utiliza break para terminar el caso.
* Puede haber un caso por defecto (default) para ejecutarse cuando no se cumple ninguno de los otros casos. El caso por defecto debe de ir al final y tampoco necesita break.

Ejemplo:

```go
package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var hora int = 10
  /*Se pasa hora como variable de prueba*/
  switch hora{
    /*Si hora coincide con alguna de las literales especificadas*/
    case 1, 2, 3, 4: fmt.Println("Aún es temprano")
    case 5, 6, 7: fmt.Println("Está atardeciendo")
    case 8: fmt.Println("Acaba de oscurecer")
    case 9, 10, 11: fmt.Println("Ya es tarde")
    default: fmt.Println("Es demasiado tarde")
  }
}
```
Resultado:

![](../Imagenes/switch-expre.png)

## Switch de tipo

El funcionamiento y estructura es similar al switch de expresiones. Las siguientes reglas aplican para switch de tipos:

* La expresión usada en una sentencia debe de ser una variable o una interface (interfaz) de tipo.
* Puede haber cualquier número de casos en un switch. Cada uno es seguido por el valor contra el cual comparar y dos puntos.
* El tipo de dato para un caso debe ser el mismo tipo de dato que la variable de prueba, así como debe de ser de un tipo válido.
* Cuando la variable de prueba es igual a un caso, se ejecuta dicho caso y los demás no son verificados. No es necesario un break.
* Puede haber un caso por defecto (default) para ejecutarse cuando no se cumple ninguno de los otros casos. El caso por defecto debe de ir al final y tampoco necesita break.

Ejemplo:

```go
package main
import "fmt"
func main() {
    
  /*variable interface sin tipo asignado*/
  var x interface{}
  switch x.(type){ /*Retorna el tipo de x*/
    /*Casos*/
    case nil: fmt.Println("Es una variable tipo nil")
    case int: fmt.Println("Es una variable tipo int")
    case float64: fmt.Println("Es una variable tipo float64")
    case int64: fmt.Println("Es una variable tipo int64")
    default: fmt.Println("No es ninguno de los tipos anteriores")
  }
} 
```
Resultado:

![](../Imagenes/switch-tipo.png)




***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).