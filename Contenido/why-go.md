# ¿Que es Go?
![](../Imagenes/go.png)

El lenguaje de programación Go fue lanzado en Noviembre 2009, desarrollado e ideado por Google. Durante este tiempo fuera del hype inicial ha surgido un moderado interés por aprender más sobre este “desconocido” lenguaje a la sombra aún de otros lenguajes ampliamente usados como Java, C++, C# o Python.

Go es un lenguaje moderno, por lo que bebe de lo mejor de muchos lenguajes. Combina una sintaxis parecida a C con las características y facilidad de lenguajes dinámicos como Python. Lenguajes como C++, Java o C# son más pesado o voluminosos. En cambio, GO acierta con una sintaxis clara y concisa.

Aún estando diseñado para la programación de sistemas, provee de un recolector de basura, reflexión, potentes patrones de concurrencia y otras capacidad de alto nivel. No está orientado a objetos porque no existe jerarquía de tipos pero implementa interfaces, lo cual nos permite un estilo de programación orientado a objetos.

Aprender Go es una buena forma de mejorar nuestras habilidades como programadores. Es interesante conocer un lenguaje bastante potente como este que quién sabe cuando podemos recurrir a él.

## Características de Go
Go, al igual que C y C++, es un lenguaje compilado y concurrente, o en otras palabras: soporta canales de comunicación basados en el lenguaje CSP. Sin embargo, la concurrencia en Go es diferente a los criterios de programación basados en bloqueos como pthreads. Los creadores de Go, además, se inspiraron en la versatilidad y las cualidades de otros lenguajes como Python, C++ y Java (entre otros), para conseguir un lenguaje con las siguientes características, algunas únicas, y otras compartidas con otros lenguajes compilados.

* Es un lenguaje compilado muy, muy rápido.
* Usa una codiﬁcación UTF-8 para todos los ﬁcheros fuente, es decir, permite usar caracteres latinos, chinos, etc.
* Usa tipado fuerte y memoria virtual segura.
* Posee punteros, pero no aritmética de los mismos.
* Es un lenguaje 100% concurrente (CSP)
* Posee hasta un servidor web incluido.
* Es OpenSource, con lo que cualquier persona puede colaborar en su desarrollo aportando ideas o implementando nuevas librerías.

## Lo bueno de Go
* Simple, confiable y eficiente: es, de algún modo, el eslogan de Go. El equipo de desarrollo de Go muchas veces ha comentado que una de sus metas fue crear un lenguaje de programación simple, fácil de aprender, leer, trabajar; y que a su vez fuera confiable.
* Concurrencia nativa: tal vez el punto fuerte de Go. Me atrevería a catalogarlo como uno de los lenguajes de programación con una de las mejores implementaciones nativas de concurrencia. La concurrencia es una propiedad de los sistemas para ejecutar diferentes procesos de forma simultánea con la posibilidad de comunicarse entre ellos. Las goroutines (las que permiten utilizar esta propiedad) son muy sencillas de aprender y con una implementación con poco que criticar.
* Excelente alternativa para el desarrollo de páginas y aplicaciones web: te sorprenderá la facilidad con la que lo puedes hacer.
* Opciones limitadas: aunque esta razón trae consigo muchos inconvenientes, realmente el lenguaje omitió tantas cosas que resulta sumamente atractivo para programadores quienes quieran un lenguaje que cumpla con su cometido y no presente problemas de compatibilidad entre infinidad de paquetes, bibliotecas, etc.
* Tu programa funcionará en cualquier actualización de la versión en la que lo escribiste: si escribiste un programa en Go 1.0, el equipo de desarrollo mantiene un acuerdo con sus usuarios: sin importar qué nuevas funciones implementen, ese programa funcionará con cualquier actualización de la versión 1.0 (1.1, 1.2, 1.3 etc.). No mantienen el acuerdo entre diferentes versiones, es decir, no te garantizan que tu programa escrito en la versión 1.0 vaya a funcionar con la versión 2.0.
* Excelente herramienta para servidores: proporciona una buena opción para sustituir a C y ensamblador en servidores ya que conserva muchas de las facilidades que estos últimos contienen, como la gestión de memoria, pero con un sintaxis más simple y fácil de aprender.
* Documentación fácil de comprender y memorizar: puedes consultar casi cualquier aspecto del lenguaje en la documentación oficial.
* Gestión de memoria sencilla: es muy sencillo consultar el código ensamblador de todo programa en tiempo de ejecución.
* Actualizaciones constantes: muchos de los problemas iniciales del lenguaje, conforme el equipo de desarrollo lanzó nuevas versiones, se han corregido. En Internet podemos encontrar infinidad de programadores que compartían su mal sabor de boca con el lenguaje, sin embargo, algunos de esos problemas ya han sido solucionados, por ejemplo: el recolector de basura en algún momento ocasionó muchos problemas de velocidad de compilación, pero esto ha sido mejorado considerablemente.

## Contras de Go
* Abstracción compleja: aunque las sentencias son tan simples que se pueden entender proyectos grandes con relativamente poca documentación, la simplicidad trajo consigo algunos problemas. Algunos algoritmos resultan muy extensos y de difícil comprensión porque muchas de las bibliotecas y funciones que otros lenguajes usaban para aligerar el trabajo del programador, no se implementaron en el lenguaje (function template, por ejemplo).
* Características no implementadas: esta desventaja es una extensión de la anterior. Algunas herramientas y características muy útiles han sido dejadas de lado con Go como la herencia, programación orientada a objetos, alias etc.
* Sin excepciones: esta es una de las características que muchos programadores han pedido incluir desde hace varias actualizaciones. Resulta un poco incómodo no poder hacer uso de excepciones en un lenguaje que es tan popular para servidores.
* El equipo de desarrollo debe de aceptar cualquier función nueva: aunque parezca razonable, su política de simplicidad y compatibilidad deja en el tintero muchas funciones que podrían llegar a ser muy atractivas y útiles porque los desarrolladores no las consideran necesarias, no han encontrado una forma “natural” de implementarla o bien porque creen que podrían traer más problemas que soluciones.

## ¿Que empresas usan Go?

* Docker, un conjunto de herramientas para desplegar contenedores Linux.
* Juju, una herramienta de orquestación de servicios de Canonical, empaquetadores de Ubuntu Linux.
* Dropbox migró algunos de sus componentes críticos de Python a Go.
* Google, para muchos proyectos, incluyendo el servidor de descarga dl.google.com.
* MercadoLibre, para varias API públicas.
* MongoDB, herramientas para administrar instancias de MongoDB.
* Netflix, para dos partes de su arquitectura de servidor.
* SoundCloud.
* Uber, para manejar grandes volúmenes de consultas basadas en geofenc






#### Referencias
* https://www.genbeta.com/desarrollo/empezar-a-aprender-go-golang
* https://keepcoding.io/es/blog/caracteristicas-lenguaje-de-programacion-go/
* https://tecnonucleous.com/2017/02/13/lenguaje-programacion-go/
***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).