package main
import "fmt"

func main(){
  nums1:=make([]int, 5, 5) //Inicializa en 0 los 5 elementos del slice
  nums1[3] = 77 //Modifica el índice 3 del slice nums1
  fmt.Println("nums1[]= ", nums1)
  nums2 := []int{9, 8, 7, 6} //Nuevo slice nums2 inicializado similar a un array
  fmt.Println("nums2[]= ", nums2)
  //Acceder a rangos (sub slices) de slices
  fmt.Println("nums2[0:2]=", nums2[:2])
  fmt.Println("nums2[2:4]=", nums2[2:])
}

