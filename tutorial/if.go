package main
import "fmt"

func main() {
  /*variable local de tipo entero*/
  var calificacion int = 5
  /*Sentencia if, que verifica si calificación es menor a 6*/
  if calificacion < 6 {
    /*Si la condición se cumple, imprime*/
    fmt.Println("Reprobaste")
  }
  fmt.Println("Tu calificación fue de: ", calificacion)
}

