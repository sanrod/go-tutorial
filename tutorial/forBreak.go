package main
import "fmt"

func main(){
  for i:=0 ; i < 10; i++ {
    fmt.Printf("Valor de i: %d", i)
    if i == 7{
      fmt.Printf(" así que saldremos del ciclo...\n")
      break
    }
    fmt.Printf("\n")
  }
}

